# Prixdog

J'avais envie de voir clairement le prix des bières sur brewdog.com, donc j'ai fait ça.

Le script _crawle_ le catalogue Brewdog, extrait les infos des produits, génère un
fichier `data.json` qui est mis en forme dans un tableau [Grid.js](https://gridjs.io).
Les données sont mises à jour une fois par semaine.

Accéder au site : [https://cnrdck.gitlab.io/prixdog/](https://cnrdck.gitlab.io/prixdog/)