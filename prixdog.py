"""Prixdog.

Génère un fichier data.json avec les prix des bières brewdog depuis
https://www.brewdog.com/fr et un fichier index.html contenant les données mises en forme.

Usage:
    ```sh
    pip install -r requirements.txt
    python prixdog.py -h
    ```
"""
import argparse
import json
import logging
import time
from dataclasses import asdict, dataclass, is_dataclass
from datetime import datetime
from functools import partial
from typing import Dict, Iterator, Optional
from urllib.error import HTTPError
from urllib.parse import urljoin
from urllib.request import Request, urlopen

from bs4 import BeautifulSoup

BASE_URL = "https://www.brewdog.com"
SHOP_PATH = "/fr/shop/all-beer"
MAX_PAGES = 20
SLEEP = 1
__VERSION__ = "20220926"

# Dictionnaire des produits, avec l'url en clé
results: Dict[str, "Product"] = {}
# Pseudo cache pour éviter les appels inutiles au site
cache: Dict[str, BeautifulSoup] = {}


logger = logging.getLogger(__name__)


@dataclass
class Product:
    """Une bière ou pas loin."""

    url: str
    name: str
    image: str
    price: float
    quantity: int
    style: Optional[str] = None
    alcohol_level: Optional[float] = None
    weight: Optional[int] = None
    price_per_litre: Optional[float] = None

    def __str__(self):
        return f"{self.name} {self.quantity} x {self.weight}ml @ {self.price}€"


class DataclassJSONEncoder(json.JSONEncoder):
    """Encodeur JSON pour convertir les dataclasses en dict."""

    def default(self, o):
        """Gestion des dataclasses."""
        if is_dataclass(o):
            return asdict(o)
        return super().default(o)


def build_request(path: str) -> Request:
    """Une requête urllib."""
    return Request(
        url=urljoin(BASE_URL, path), headers={"User-Agent": f"Prixdog/{__VERSION__}"}
    )


def fetch_page(url: str) -> BeautifulSoup:
    """Récupération d'une page.

    La page est retournée sous forme de BeautifulSoup.
    Un pseudo mécanisme de cache permet de ne pas récupérer plusieurs fois la même page.
    Après chaque GET on pause `SLEEP` secondes pour ne pas surcharger le serveur.

    Raises:
        HTTPError: en cas d'échec de l'appel `urlopen`.

    Returns:
        Un objet `BeautifulSoup`.
    """
    request = build_request(url)
    if cached_soup := cache.get(request.full_url):
        return cached_soup
    try:
        handle = urlopen(request)
        time.sleep(SLEEP)
    except HTTPError as error:
        print(error.reason)
        raise error

    soup = BeautifulSoup(handle, features="html.parser")
    cache[request.full_url] = soup
    return soup


def get_main_product_pages_urls() -> Iterator[str]:
    """Retourne les urls des produits du catalogue.

    Yields:
        url d'une page produit.
    """
    for page in range(1, MAX_PAGES):
        url = f"/fr/shop/all-beer?page={page}"
        soup = fetch_page(url)
        logger.info("Page boutique %s", url)
        for link in soup.select('a[data-testid="product-card"]'):
            product_url = urljoin(BASE_URL, link.get("href"))
            logger.info("Trouvé url produit %s", product_url)
            yield product_url


def extract_products(url: str) -> Iterator[Product]:
    """Extraction des infos produit.

    Il peut y avoir plusieurs variantes de produit sur une page (pack de 4, 12, etc.)

    Args:
        url: l'url de la page produit principale

    Yields:
        Un Product
    """

    logger.info("Début extraction %s", url)
    soup = fetch_page(url)
    name = soup.find("h2").get_text()
    logger.info("Produit %s", name)
    try:
        image = soup.select_one('img[class="rounded-lg"]').get("src")
    except AttributeError:  # Image non trouvée
        image = None
    style = get_style(soup)
    alcohol_level = get_alcohol_level(soup)

    variants = soup.select('a[class="block"]')
    if not variants:
        variants = soup.find_all(attrs={"data-testid": "product-varient-0"})

    for variant in variants:
        if variant.get("href"):
            product_url = urljoin(BASE_URL, variant.get("href"))
        else:
            product_url = url

        logger.info("Variante %s", product_url)

        if results.get(product_url):
            logger.info("Déjà récupéré")
            continue

        price = get_price(variant)
        try:
            product_soup = fetch_page(product_url)
        except HTTPError as error:
            logger.info("Erreur %s lors de la récupération de %s", error, product_url)
            continue
        quantity = get_quantity(product_soup)
        weight = get_weight(product_soup)
        alcohol_level = get_alcohol_level(product_soup)

        product = Product(
            url=product_url,
            name=name,
            image=image,
            price=price,
            quantity=quantity,
            style=style,
            alcohol_level=alcohol_level,
            weight=weight,
        )
        calc_price_per_litre(product)
        results[product.url] = product
        logger.info("Extrait %s", product)
        yield product


def calc_price_per_litre(product: Product):
    if product.weight:
        product.price_per_litre = round(
            product.price / (product.quantity * product.weight / 1000), 2
        )


def get_weight(soup: BeautifulSoup) -> Optional[int]:
    """Extraction de la contenance

    Args:
        soup: la soupe

    Returns:
        La contenance (si disponible)
    """
    label = soup.find("p", string="Quantité")
    # 24 x canettes (330ml) ou 24 canettes + 1 verre ou 48 canettes (330ml)
    value = label.next_sibling.get_text().split()[-1]

    if value.startswith("("):
        weight = value.replace("(", "").replace(")", "")
        if "oz" in weight:  # sympa d’utiliser les onces des fois Brewdog
            return round(29.5735 * int(weight.replace("oz", "")))
        else:
            return int(weight.replace("ml", ""))
    return None


def get_quantity(soup: BeautifulSoup) -> int:
    """Extraction de la quantité

    Args:
        soup: la soupe

    Returns:
        La quantité
    """
    label = soup.find("p", string="Quantité")
    # 24 x canettes (330ml) ou 6 x Can (330ml) ou 24 canettes + 1 verre
    description = label.next_sibling.get_text().split()
    return int(description[0])


def get_style(soup: BeautifulSoup) -> Optional[str]:
    """Extraction du style de bière

    Args:
        soup: la soupe

    Returns:
        Le style (si disponible)
    """
    if label := soup.find("p", string="Style de bière"):
        return label.next_sibling.get_text()
    return None


def get_alcohol_level(soup: BeautifulSoup) -> Optional[float]:
    """Extraction du degré d'alcool

    Args:
        soup: la soupe

    Returns:
        Le degré d'alcool (si disponible)
    """
    if label := soup.find("p", string="Degré"):
        return float(label.next_sibling.get_text().replace("%", ""))
    return None


def get_price(soup: BeautifulSoup) -> float:
    """Extraction du prix

    Args:
        soup: la soupe

    Returns:
        Le prix
    """
    description = soup.select_one('span[class="font-medium"]').get_text().split()
    return float(description[0].replace(",", "."))


def fetch_data():
    """Récupération des données sur le site brewdog.

    Récupère la liste des produits sur le catalogue, consulte les page, extrait les
    infos et stocke le tout dans `data.json`.
    """
    print("Récupération des données")
    for url in get_main_product_pages_urls():
        print("Page produit :", url)
        for product in extract_products(url):
            print("Trouvé :", product)

    logger.info("Génération de data.json")
    with open("public/data.json", "w+", encoding="utf-8") as handle:
        json.dump(
            list(results.values()),
            handle,
            ensure_ascii=False,
            indent=4,
            cls=DataclassJSONEncoder,
        )


def serve():
    import http.server
    import socketserver

    handler = partial(http.server.SimpleHTTPRequestHandler, directory="public")

    with socketserver.TCPServer(("", 8000), handler) as httpd:
        print("Serveur lancé sur http://localhost:8000 ctrl+c pour arrêter")
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            httpd.server_close()
            print("Boujou")


def generate_page():
    with open("template.html") as handle:
        template = handle.read()
    now = datetime.now()
    page = template.replace("{{VERSION}}", __VERSION__).replace(
        "{{DATE}}", now.strftime("%d/%m/%Y")
    )
    with open("public/index.html", "w+", encoding="utf-8") as handle:
        handle.write(page)


if __name__ == "__main__":
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    parser = argparse.ArgumentParser(
        prog="prixdog",
        description="Récupère les prix sur brewdog.com/fr.",
        epilog="Le résultat est dans public/data.json et index.html",
    )

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version=f"{parser.prog} version {__VERSION__}",
    )

    parser.add_argument(
        "-d",
        "--skip-data",
        action="store_true",
        help="ne pas récupérer les prix sur le site (utiliser un data.json existant)",
    )
    parser.add_argument(
        "-s",
        "--serve",
        action="store_true",
        help="servir la page",
    )
    args = parser.parse_args()

    if not args.skip_data:
        fetch_data()

    generate_page()

    if args.serve:
        serve()
